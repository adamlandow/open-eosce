<?php

namespace App\Http\Controllers;

use App\Exam_instance;
use App\Exam_instance_item;
use App\Exam_instance_item_item;
use App\Group;
use App\Http\Requests;
use App\Student;
use App\Student_exam_submission;
use App\Student_exam_submission_item;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;


/**
 * Class StudentExamSubmissionController
 * @package App\Http\Controllers
 * Controller for the submission of examinations
 * API access only, no frontend at this point
 */
class StudentExamSubmissionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        //@TODO filter by archived and template
        return null;
    }

    public function show($id)
    {

        return null;

    }

    public
    function update(Request $request)
    {
return null;
    }

/////////////////////////////////////////////////////////////////////////////////////
///
/// Submission is good
///
/// ////////////////////////////////////////////////////////////////////////////////
///

    /**
     * Accept the submission of an assessment
     * @param Request $request
     * @return array
     */
    public
    function submitAssessment(Request $request)
    {
        $input = $request::all();
        //@TODO check that this hasn't been already submitted
        $submitdata = json_decode($input['submitdata']);
        //   dd($submitdata);
        $submission = Student_exam_submission::create(['student_id' => $submitdata->student_id, 'exam_instances_id' => $submitdata->exam_instances_id, 'comments' => $submitdata->comments, 'created_by' => Auth::user()->id]);

        $answers = [];
        foreach ($submitdata->answerdata as $answer) {
            if (strval(Exam_instance_item::find($answer->id)->heading) != '1') {
                $answers[] = new Student_exam_submission_item(['exam_instance_items_id' => $answer->id, 'selected_exam_instance_items_items_id' => $answer->selected_id, 'comments' => ((isset($answer->comment) ? $answer->comment : ''))]);
            }
        }
        $submission->student_exam_submission_items()->saveMany($answers);

        $response = array(
            'status' => 0,
        );
        return $response;
    }


}
