<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

// authentication
Auth::routes();

// hack to get logout working as GET
Route::get('/logout', 'Auth\LoginController@logout');

// LDAP testing


// Users
Route::resource('user', 'UserController');

Route::get('user/{}', 'UserController@show')->name('user.show');

Route::get('/my', 'UserController@my')->name('user.my');

Route::post('user/ajaxupdate', 'UserController@ajaxupdate');
Route::post('user/ajaxstore', 'UserController@ajaxstore');
Route::post('user/ajaxdestroy', 'UserController@ajaxdestroy');
Route::post('user/activate', 'UserController@activate');

//search for a user support for select2
Route::post('user/select2search', 'UserController@select2search');

// User images
Route::get('user/{id}/image', 'UserImageController@display');
Route::get('user/thumb/{id}/{size}', 'UserImageController@thumb');

// make the user image controller
Route::resource('userimage', 'UserImageController');

// update a media to a user
Route::post('userimage/update', 'UserImageController@update');

/////////////////////////////////////////////////
// User switching
/////////////////////////////////////////////////
Route::post('/sudosu/login-as-user', 'UserController@loginAsUser')
    ->name('sudosu.login_as_user');

Route::post('/sudosu/return', 'UserController@returnToUser')
    ->name('sudosu.return');


//////////////////////////////////////////////////
//Setup pages/ routes
/////////////////////////////////////////////////

Route::get('setup', 'SetupController@view')->name('setup.index');
Route::get('setup/configqrimage', 'SetupController@showConfigQR');


/////////////////////////////////////////////////
//students
/////////////////////////////////////////////////
Route::resource('student', 'StudentController');

Route::get('student/show/{id}', 'StudentController@show')->name('student.show');;
Route::post('student/ajaxupdate', 'StudentController@ajaxupdate');
Route::post('student/ajaxstore', 'StudentController@ajaxstore');
Route::post('student/ajaxdestroy', 'StudentController@ajaxdestroy');

//search for a student support for select2
Route::post('student/select2search', 'StudentController@select2search');

// student photos
// add a photo to a student
Route::post('student_image/create', 'StudentImageController@createmedia');
// update a image to a student
Route::post('student_image/update', 'StudentImageController@update');
// make the media controller a resource
Route::resource('student_image', 'StudentImageController');
// show the actual image (or an icon if a document)
Route::get('/student_image/show/{id}', 'StudentImageController@display');
// force a download
Route::get('/student_image/download/{id}', 'StudentImageController@download');
// show a thumbnail
Route::get('/student_image/thumb/{id}/{size}', 'StudentImageController@thumb');

// Examination Preparation and Administration
// show exams
Route::resource('exam', 'ExamInstanceController');
Route::post('exam/ajaxupdate', 'ExamInstanceController@ajaxupdate');
Route::post('exam/ajaxstore', 'ExamInstanceController@ajaxstore');
Route::post('exam/ajaxdestroy', 'ExamInstanceController@ajaxdestroy');
Route::get('exam/{id}/preview', 'PublicExamInstanceController@show');

// Assessment assessor management
Route::post('exam/addassessors', 'ExamInstanceController@addassessors');
Route::post('exam/removeassessors', 'ExamInstanceController@removeassessors');

// Assessment candidate management
Route::post('exam/addcandidates', 'ExamInstanceController@addcandidates');
Route::post('exam/addcandidatesbycsv', 'ExamInstanceController@addcandidatesbycsv');
Route::post('exam/updatecandidategroup', 'ExamInstanceController@updatecandidategroup');
Route::post('exam/removecandidate', 'ExamInstanceController@removecandidates');

// templates
Route::get('examtemplates', 'ExamInstanceController@templateindex')->name('examtemplates.index');
Route::get('examtemplates/{id}', 'ExamInstanceController@templateshow')->name('examtemplates.show');
Route::post('examtemplates/ajaxstore', 'ExamInstanceController@templatestore');
Route::post('examtemplates/{id}/ajaxupdate', 'ExamInstanceController@templateajaxupdate');

Route::get('examitemtemplates', 'ExamInstanceItemController@templateindex')->name('examitemtemplates.index');
Route::post('examitemtemplates/ajaxstore', 'ExamInstanceItemController@templatestore');
Route::post('examitemtemplates/{id}/ajaxupdate', 'ExamInstanceItemController@update');


// Assessment item management
Route::resource('examitem', 'ExamInstanceItemController');
Route::post('examitem', 'ExamInstanceItemController@update');
Route::post('examitem/store', 'ExamInstanceItemController@store');
Route::post('examitem/update', 'ExamInstanceItemController@update');
Route::post('examitem/ajaxdestroy', 'ExamInstanceItemController@ajaxdestroy');
Route::post('examitem/reorder', 'ExamInstanceItemController@reorder');
Route::post('examitem/{id}/getitemitemsasarray', 'ExamInstanceItemController@getitemitemsasarray');

// media for examinations
// make the media controller a resource
Route::resource('exammedia', 'ExaminationInstancesMediaController');
// add a media to a exam
Route::post('exammedia/create', 'ExaminationInstancesController@createmedia');
// update a media to a exam
Route::post('examinationmedia/update', 'ExaminationInstancesMediaController@update');
// show the actual image (or an icon if a document)
Route::get('examinationmedia/show/{id}', 'ExaminationInstancesMediaController@display');
// force a download
Route::get('examinationmedia/download/{id}', 'ExaminationInstancesMediaController@download');
// show a thumbnail
Route::get('examinationmedia/thumb/{id}', 'ExaminationInstancesMediaController@thumb');


// Reporting
Route::resource('report', 'ExamReportsController');
// show an individual session
Route::get('report/session/{sessionid}', 'ExamReportsController@detail')->name('report.session');

// updating
Route::resource('submissionitem', 'StudentExamSubmissionItemController');
Route::post('submissionitem/update', 'StudentExamSubmissionItemController@update');

// item changelog
Route::get('submissionitem/changelog/{id}', 'StudentExamSubmissionItemController@changelog');

// Conducting assessments in-site
Route::get('assess/{id}', 'ExamInstanceController@showInternalExam')->name('assess');